# FastApi-CRUD
CRUD application made using FastAPI and SQLAlchemy

Firstly install the requirements
Secondly start the server using the command - uvicorn main:app --reload
Go to http://127.0.0.1:8000/docs to Manipulate the API operations
