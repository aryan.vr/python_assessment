from http.client import HTTPException
import models
from fastapi import FastAPI, Depends
from sqlalchemy import and_
from sqlalchemy.orm import Session
from database import SessionLocal, engine
from pydantic import BaseModel
import uuid

models.Base.metadata.create_all(bind=engine)

app = FastAPI (
    title = "Address API",
    description = "Adds, Displays, Retrieves",
    version = "1.0",
)

class AddressSchema(BaseModel):
    address: str
    latitude: float
    longitude: float
    class Config:
        orm_mode=True

def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

@app.get("/addressapi/")
async def Getaddress(db:Session=Depends(get_db)):
    return db.query(models.Address).all()


@app.post("/addressapi/",response_model = AddressSchema)
async def Postaddress(address:AddressSchema, db:Session=Depends(get_db)):
    obj = models.Address(address=address.address,latitude=address.latitude,longitude=address.longitude)
    db.add(obj)
    db.commit()
    return obj

@app.post("/addressapi/",response_model = AddressSchema)
async def Postaddress(address:AddressSchema, db:Session=Depends(get_db)):
    obj = models.Address(address=address.address,latitude=address.latitude,longitude=address.longitude)
    db.add(obj)
    db.commit()
    return obj

@app.put("/retrieve/{latitude}/{longitude}")
async def RetrieveAddress(latitude:float,longitude:float, db:Session=Depends(get_db)):
    try:
        u = db.query(models.Address).filter(and_(latitude<=latitude,longitude<=longitude)).all()
        return u
    except:
        return HTTPException(status_code=404,details="no data")



