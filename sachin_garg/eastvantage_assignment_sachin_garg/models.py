
from sqlalchemy import Boolean, Column, ForeignKey, Numeric, Integer, String
from sqlalchemy.orm import relationship

from database import Base

class Address(Base):
    __tablename__ = "Address"

    id = Column(Integer, primary_key=True, index=True)
    address = Column(String, unique=True, index=True)
    latitude = Column(Numeric(9, 6))
    longitude = Column(Numeric(9, 6))
